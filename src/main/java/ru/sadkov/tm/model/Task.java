package ru.sadkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import ru.sadkov.tm.model.enumerate.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public final class Task extends AbstractEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private Project project;


    public Task(@NotNull final String id, @NotNull final String name, @NotNull final Project project, @NotNull final String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.project = project;
        this.dateEnd = new Date();
        this.dateBegin = new Date();
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    public String toString() {
        return "Task{" +
                "projectId='" + project.getId() + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", status=" + status +
                '}';
    }
}
