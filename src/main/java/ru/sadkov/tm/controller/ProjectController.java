package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.service.IUserService;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @NotNull
    @Autowired
    IUserService userService;

    @GetMapping("/")
    public String index(@NotNull final Model model) {
        return "index";
    }

    @GetMapping("/projects")
    public String projects(@NotNull Model model) {
        @NotNull List<Project> projects = projectService.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

    @RequestMapping(value = "/project-create/{id}", method = RequestMethod.GET)
    public String createProjectGet(Model model, @PathVariable(name = "id") String id,
                                   @ModelAttribute("project") Project project,
                                   BindingResult result) {
        System.out.println(id);
        System.out.println(project.getId());
        model.addAttribute("userId", id);
        model.addAttribute("projectId", project.getId());
        return "projectCreate";
    }

    @RequestMapping(value = "/start/{id}", method = RequestMethod.GET)
    public String startProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.startProject(project.getName());
        return "redirect:/projects/" + project.getUser().getLogin();
    }

    @RequestMapping(value = "/end/{id}", method = RequestMethod.GET)
    public String endProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.endProject(project.getName());
        return "redirect:/projects/" + project.getUser().getLogin();
    }

    @RequestMapping(value = "/project-create/{id}", method = RequestMethod.POST)
    public String createProjectPost(@PathVariable(name = "id") String id, @ModelAttribute("name") final String name, @ModelAttribute("description") final String description, BindingResult result) {
        projectService.persist(id, name, description);
        return "redirect:/projects/" + userService.findOneById(id).getLogin();
    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public String viewProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "project";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String removeProject(@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.remove(id);
        return "redirect:/projects/" + project.getUser().getLogin();
    }

    @RequestMapping(value = "/home")
    public String backToProjectList(Model model) {
        return "redirect:/projects";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "editProject";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editProjectPost(@ModelAttribute("project") Project project) {
        @NotNull final Project fullProject = projectService.findOne(project.getId());
        projectService.update(project.getId(), project.getName(), project.getDescription());
        return "redirect:/projects/" + fullProject.getUser().getLogin();
    }

    @RequestMapping(value = "/projects/{login}", method = RequestMethod.GET)
    public String projectsForUser(Model model, @PathVariable("login") String login) {
        @NotNull List<Project> projects = projectService.findProjectsByUser(login);
        model.addAttribute("projectList", projects);
        User user = userService.findOneByLogin(login);
        model.addAttribute("user", user);
        return "projects";
    }

    @GetMapping(value = "/developer")
    public String developer() {
        return "developer";
    }

}
