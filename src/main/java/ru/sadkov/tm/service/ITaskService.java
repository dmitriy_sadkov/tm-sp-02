package ru.sadkov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.List;

public interface ITaskService {


    @Nullable Task findTaskByName(@Nullable final String taskName);

    @Transactional
    boolean saveTask(@Nullable String taskName, @Nullable String projectName, @Nullable String description);

    void removeTask(@Nullable final String taskName);

    @Nullable
    List<Task> findAll();

    void removeAll();

    void removeTaskForProject(@Nullable final String projectName);

    @Nullable
    List<Task> getTasksByPart(@Nullable final String part);

    @Nullable
    List<Task> findTasksByStatus(@Nullable final Status status);

    @Nullable
    String startTask(@Nullable final String taskName);

    @Nullable
    String endTask(@Nullable final String taskName);

    void persist(@Nullable final Task task);

    void clear();

    void load(@Nullable final List<Task> tasks);

    List<Task> findAllByProjectId(String id);

    Task findOneById(String id);

    void removeById(String id);

    void update(String id, String name, String description);
}
